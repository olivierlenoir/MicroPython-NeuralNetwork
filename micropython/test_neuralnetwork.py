# Author: Olivier Lenoir <olivier.len02@gmail.com>
# Created: 2021-02-10 18:25:54
# License: MIT, Copyright (c) 2021 Olivier Lenoir
# Language: Python 3.7.3
# Project: Test Neural Network
# Description:

from matrix import Matrix
from neuralnetwork import DFF


def short(a):
    return round(a, 3)


training_set = [
    [Matrix([[0, 0, 0]]), Matrix([[1, 0]])],
    [Matrix([[0, 0, 1]]), Matrix([[0, 1]])],
    [Matrix([[0, 1, 1]]), Matrix([[0, 1]])],
    [Matrix([[0, 1, 0]]), Matrix([[1, 0]])],
    [Matrix([[1, 1, 0]]), Matrix([[0, 1]])],
    [Matrix([[1, 1, 1]]), Matrix([[1, 0]])],
    [Matrix([[1, 0, 1]]), Matrix([[0, 1]])],
    [Matrix([[1, 0, 0]]), Matrix([[1, 0]])],
    ]

nn = DFF((3, 4, 2))

print('Intitial weigths')
for w in nn.weights:
    print(w.map(short))

print('=' * 20)
print('Learning progress')
for i in range(1000):
    for a, s in training_set:
        nn.train(a, s, 1)
    if i % 10 == 0:
        print('.', end='')

print()
print('=' * 20)
score = True
for a, s in training_set:
    p = nn.predict(a)
    scr = str(p.map(round)) == str(s.map(short))
    print(a.map(short), p.map(short), p.map(round), s.map(short), scr)
    score &= scr
print('Good learning?', score)

print('=' * 20)
print('Rounded weights')
for i, w in enumerate(nn.weights):
    print('W{}'.format(i), w.map(round))

nn.dump('test_neuralnetwork.json')
