# Author: Olivier Lenoir <olivier.len02@gmail.com>
# Created: 2023-01-15 15:15:44
# License: MIT, Copyright (c) 2023-2024 Olivier Lenoir
# Project: pdflatex

.PHONY: all
.SILENT: all
all: pdf


.PHONY: help
.SILENT: help
help:  # Show help for each of the Makefile recipes.
	@grep -E '^[a-zA-Z0-9 -]+:.*#'  Makefile | sort | while read -r l; do printf "\t\033[1;32m$$(echo $$l | cut -f 1 -d':')\033[00m:$$(echo $$l | cut -f 2- -d'#')\n"; done


.PHONY: pdf
.SILENT: pdf
pdf: clean gv  # Build PDF from LaTeX files
	for file in *.tex; do \
		echo $${file}; \
		latexmk -pdf -pdflatex="pdflatex --shell-escape" $${file}; \
		latexmk -c; \
		done
	make clean


.PHONY: clean
.SILENT: clean
clean:
	rm -f *.aux
	rm -f *.bbl
	rm -f *.blg
	rm -f *.lof
	rm -f *.log
	rm -f *.lot
	rm -f *.nav
	rm -f *.out
	rm -f *.toc
	rm -f *-gnuplottex-fig*.*
	rm -f *.gnuploterrors
	rm -f graphviz/*.pdf


.PHONY: gv
.SILENT: gv
gv:  # Build PDF from GraphViz files
	dot -Tpdf graphviz/NeuralNetworkDFF342.gv > graphviz/NeuralNetworkDFF342.pdf
	dot -Tpdf graphviz/NeuralNetworkDFF342Detail.gv > graphviz/NeuralNetworkDFF342Detail.pdf
	dot -Tpdf graphviz/NeuralNetworkDFF3452.gv > graphviz/NeuralNetworkDFF3452.pdf
